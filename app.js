/*
 * app.js - ルーティングを備えたExpressサーバー
 */

/*jslint         browser : true, continue : true,
  devel  : true, indent  : 2,      maxerr : 50,
  newcap : true, nomen   : true, plusplus : true,
  regexp : true, sloppy  : true,     vars : false,
  white  : true
*/
/*global */

//------------ モジュールスコープ変数開始 ------------
'use strict';
var
    http = require('http'),
    express = require('express'),
    routes = require('./lib/routes'),
    logger = require('morgan'),
    bodyParser = require('body-parser'),
    errorHandler = require('errorhandler'),
    methodOverride = require('method-override'),
    basicAuth = require('basic-auth-connect'),

    app = express(),
    server = http.createServer(app);
//------------ モジュールスコープ変数終了 ------------

//------------ サーバー構成開始 ------------
// フォームのデコード
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.use(methodOverride()); // RESTfulインターフェース用
app.use(express.static(__dirname + '/public')); // 静的ファイルの配信

if (app.get('env') === 'development') {
    app.use(logger('combined')); // 引数はログの形式
    app.use(errorHandler({
        dumpExceptions: true,
        showStack: true
    }));
} else if (app.get('env') === 'production') {
    app.use(express.errorHandler());
}

routes.configRoutes(app, server);
//------------ サーバー構成終了 ------------

//------------ サーバー起動開始 ------------
server.listen(3000);
console.log(
    'Express server listening on port %d in %s mode',
    server.address().port, app.settings.env
);
//------------ サーバー起動終了 ------------